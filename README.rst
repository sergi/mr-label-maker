Mr Label Maker
==============

Script for automatic labeling of issues and merge requests for projects
hosted on gitlab.freedesktop.org.

Dependencies:

- python3
- pip3
- pyyaml

Run instructions:

``Mr Label Maker`` can work off a local configuration file (recommended) or work
with one of the built-in project specs (deprecated).
The default lookup paths for the configuration file are `.mr-label-maker.yml` and
`.gitlab-ci/mr-label-maker.yml`, whichever is found first.
See the ``example-mr-label-maker.yml`` file for a reference to the file format.

.. code-block:: sh

  # install from a local checkout
  $ pip3 install --user .

  # or install from the git repo directly
  $ pip3 install --user git+http://gitlab.freedesktop.org/freedesktop/mr-label-maker

  # to run using .mr-label-maker.yml (or .gitlab-ci/mr-label-maker.yml)
  $ ~/.local/bin/mr-label-maker --merge-requests --issues --token "$TOKEN" --dry-run

  # to run using a URL as config file
  $ ~/.local/bin/mr-label-maker --merge-requests --issues --token "$TOKEN" --dry-run --config https://foo.com/bar.yml

  # to run using a built-in project spec, mesa in this case
  $ ~/.local/bin/mr-label-maker --project mesa --merge-requests --issues --token "$TOKEN" --dry-run

## Access tokens

By default, Mr Label Maker uses the `GITLAB_TOKEN` environment variable as token value.
A custom token can be specified with the `--token` argument. If none is specified and
the environment variable is unset, Mr Label Maker looks up the token in the
`$XDG_CONFIG_HOME/mr-label-maker/tokens.toml` file. This file should have in this form:

```toml
["https://gitlab.freedesktop.org"]
token = "abcdefsometokenvalue"

["https://gitlab.example.com"]
token = "1235sometokenvalue"
```
