#!/usr/bin/env python3

# Copyright © 2020-2022 Intel Corporation

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from enum import IntEnum
from pathlib import Path
import argparse
import datetime
import os
import sys
import time

from .project import GitLabError, GitLabProject, ProjectConfig


class ExitCode(IntEnum):
    SUCCESS = 0
    USER_ERROR = 1
    API_ERROR = 2  # Note: also used by Argparse as usage error
    BUG = 50


def load_token_from_toml(instance: str) -> str | None:
    try:
        import tomllib

        xdg_config_home = Path(os.getenv("XDG_CONFIG_HOME", Path.home() / ".config"))
        tomlpath = Path("mr-label-maker") / "tokens.toml"
        tomlfile = xdg_config_home / tomlpath
        try:
            with open(tomlfile, "rb") as fd:
                data = tomllib.load(fd)
                return data[instance]["token"]
        except FileNotFoundError:
            pass
        except KeyError:
            print(f"Failed to find an entry for this project in {tomlfile}")
        except tomllib.TOMLDecodeError as e:
            print(f"Failed to decode {tomlfile}: {e}")

    except ModuleNotFoundError:
        pass
    return None


def main(argv=sys.argv[1:]) -> ExitCode:
    parser = argparse.ArgumentParser(
        description="Label issues and merge requests on GitLab"
    )
    parser.add_argument(
        "--dry-run",
        "-d",
        action="store_const",
        const=True,
        default=False,
        help="don't apply any changes",
    )
    parser.add_argument(
        "--config",
        required=False,
        default=None,
        type=str,
        help='Path or URL to the mr-label-maker.yml config file"',
    )
    parser.add_argument(
        "--token",
        "-t",
        default=os.environ.get("GITLAB_TOKEN"),
        type=str,
        help="set GitLab API token",
    )
    parser.add_argument(
        "--issues",
        "-i",
        nargs="?",
        default=0,
        const=-1,
        type=int,
        help="process one specific issue id (if given) or all issues matching the --state",
    )
    parser.add_argument(
        "--merge-requests",
        "-m",
        "--mrs",
        nargs="?",
        default=0,
        const=-1,
        type=int,
        help="process one specific merge request id (if given) or all merge requests matching the --state",
    )
    parser.add_argument(
        "--label",
        "-l",
        nargs=1,
        default=None,
        type=str,
        help="search for issues/MRs with this label (default=%(default)s)",
    )
    parser.add_argument(
        "--state",
        "-s",
        default="opened",
        choices=["opened", "closed", "merged", "all"],
        type=str,
        help="search for issues/MRs in this state (default=%(default)s)",
    )
    parser.add_argument(
        "--ignore-label-history",
        action="store_const",
        const=True,
        default=False,
        help="process issues with a history of label changes (default=%(default)s)",
    )
    parser.add_argument(
        "--poll", default=0, type=int, help="poll GitLab every POLL seconds"
    )
    parser.add_argument(
        "--since",
        default=0,
        type=int,
        help="only look at issues/merge requests that were updated in the last --since minutes",
    )
    parser.add_argument(
        "--create-labels",
        action="store_const",
        const=True,
        default=False,
        help="create any new labels",
    )

    args = parser.parse_args(argv)

    if not args.issues and not args.merge_requests:
        print(
            "Nothing to process, please provide either --issues and/or --merge-requests"
        )
        return ExitCode.USER_ERROR

    if args.since > 0:
        now = datetime.datetime.now(datetime.timezone.utc)
        since = now - datetime.timedelta(minutes=args.since)
    else:
        since = None

    if (config_file := args.config) is None:
        DEFAULT_PATHS = (".mr-label-maker.yml", ".gitlab-ci/mr-label-maker.yml")
        for dflt in DEFAULT_PATHS:
            if Path(dflt).exists():
                config_file = dflt
                break
        else:
            print(
                f"Unable to find default mr-label-maker.yml file, tried {', '.join(DEFAULT_PATHS)}"
            )
            return ExitCode.USER_ERROR

    if config_file.startswith("http://") or config_file.startswith("https://"):
        import urllib.error
        import urllib.request

        for _ in range(3):
            try:
                with urllib.request.urlopen(config_file) as response:
                    config = ProjectConfig.from_yaml(response.read())
                    break
            except (urllib.error.HTTPError, urllib.error.URLError) as e:
                print(f"Failed to fetch config file: {e}")
                time.sleep(3)
        else:
            print("Unable to fetch config file, exiting now.")
            return ExitCode.USER_ERROR
    else:
        if not Path(config_file).exists():
            print(f"Config file {config_file} not found in CWD")
            return ExitCode.BUG
        config = ProjectConfig.from_yaml(open(config_file))

    token = args.token
    if not token:
        token = load_token_from_toml(config.url)
        if not token and not args.dry_run:
            print("Missing GitLab API token. Use $GITLAB_TOKEN or --token.")
            return ExitCode.USER_ERROR

    proj = GitLabProject(
        config=config,
        token=token,
        dry_run=args.dry_run,
        label=args.label or [],
        state=args.state,
        ignore_label_history=args.ignore_label_history,
    )

    try:
        proj.connect()
    except GitLabError as e:
        print(e)
        return ExitCode.API_ERROR

    if not args.create_labels:
        labels = config.labels
        project_labels = proj.fetch_labels()

        missing = [f"'{l}'" for l in labels if l not in project_labels]
        if missing:
            print(f"Labels that do not exist in the project: {', '.join(missing)}")
            print("Use --create-labels to force-create them")
            return ExitCode.USER_ERROR

    while True:
        if args.issues:
            proj.process_issues(issue_id=args.issues, since=since)

            if args.merge_requests:
                print("\n\n")

        if args.merge_requests:
            proj.process_mrs(mr_id=args.merge_requests, since=since)

        if args.poll == 0:
            break
        time.sleep(args.poll)

    return ExitCode.SUCCESS


if __name__ == "__main___":
    exit_code = main()
    if exit_code != ExitCode.SUCCESS:
        sys.exit(exit_code)
