# Copyright © 2020-2022 Intel Corporation

# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:

# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.

# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

from typing import Callable, Iterable, Sequence, TypeAlias
from dataclasses import dataclass

import datetime
import re
import time

# from python-gitlab package
import gitlab

LabelMap: TypeAlias = dict[str, str | Sequence[str]]
LabelPatternMap: TypeAlias = dict[re.Pattern, str | Sequence[str]]
TextFunc: TypeAlias = Callable[[str], str]


def escape_term(string):
    return "".join(filter(lambda x: ord(x) >= 32 or ord(x) in {9, 10}, string))


def set2str(s):
    return ", ".join(sorted(s))


def flatten(m: LabelMap | LabelPatternMap) -> Iterable[str]:
    for v in m.values():
        if isinstance(v, str):
            yield v
        else:
            assert isinstance(v, list)
            for e in v:
                yield e


@dataclass
class ProjectConfig:
    @dataclass
    class ConfigSet:
        areas: LabelMap
        title_codenames: LabelMap
        desc_codenames: LabelMap
        paths: LabelPatternMap

        @property
        def maps(self) -> list[LabelMap | LabelPatternMap]:
            return [self.areas, self.title_codenames, self.desc_codenames, self.paths]

    gitlab_id: int
    name: str | None
    issues: ConfigSet
    mrs: ConfigSet
    url: str = "https://gitlab.freedesktop.org"

    @classmethod
    def from_yaml(cls, stream) -> "ProjectConfig":
        import yaml

        yml = yaml.safe_load(stream)
        assert yml["version"] == 1

        issues = ProjectConfig.ConfigSet(
            areas=yml.get("issues", {}).get("topics", {}),
            title_codenames=yml.get("issues", {}).get("titles", {}),
            desc_codenames=yml.get("issues", {}).get("descriptions", {}),
            paths={},
        )
        mrs = ProjectConfig.ConfigSet(
            areas=yml.get("merge_requests", {}).get("topics", {}),
            title_codenames=yml.get("merge_requests", {}).get("titles", {}),
            desc_codenames=yml.get("merge_requests", {}).get("descriptions", {}),
            paths={
                re.compile(k): v
                for k, v in yml.get("merge_requests", {}).get("paths", {}).items()
            },
        )

        kwargs = {
            "gitlab_id": yml["project"]["id"],
            "name": yml["project"].get("name"),
            "issues": issues,
            "mrs": mrs,
        }

        url = yml["project"].get("instance")
        if url:
            kwargs["url"] = url

        return cls(**kwargs)

    @property
    def labels(self) -> list[str]:
        maps = [*self.issues.maps, *self.mrs.maps]
        labels = {l for m in maps for l in flatten(m)}
        return sorted(labels)


class GitLabError(Exception):
    def __init__(self, message: str):
        self.message = message


class GitLabProject:
    def __init__(
        self,
        config: ProjectConfig,
        token: str,
        dry_run: bool = False,
        label: list[str] | None = None,
        state: str = "opened",
        ignore_label_history: bool = False,
    ):
        self.config = config
        self.token = token
        self.dry_run = dry_run
        self.label: list[str] = label or []
        self.state = state
        self.ignore_label_history = ignore_label_history

    def connect(self):
        try:
            self.gl = gitlab.Gitlab(self.config.url, self.token)
        except gitlab.GitlabError as ex:
            raise GitLabError(
                f"Unable to create GitLab server connection: {ex}"
            ) from ex

        try:
            self.proj = self.gl.projects.get(self.config.gitlab_id)
        except gitlab.GitlabError as ex:
            raise GitLabError(f"Unable to get information about project: {ex}") from ex

    def apply_labels(self, issue, labels):
        if issue.labels:
            print("old labels: " + set2str(issue.labels))
        print("new labels: " + set2str(labels))

        if self.dry_run:
            print('skipping because "dry run" mode is enabled')
            return

        for l in labels:
            issue.labels.append(l)

        # remove any 'mr-label-maker::*' label that was manually applied
        for l in ["mr-label-maker::relabel"]:
            if l in issue.labels:
                issue.labels.remove(l)

        issue.save()
        print("applied")

    @staticmethod
    def add_labels(dst: set, src: str | Sequence[str]):
        if isinstance(src, str):
            if src == "":
                print("empty label")
                raise ValueError("Empty label")
            dst.add(src)
        elif isinstance(src, list):
            for s in src:
                dst.add(s)
        else:
            raise ValueError("unhandled type " + str(type(src)))

    @staticmethod
    def match_starts_with(
        labels: set, func: TextFunc, map_: LabelMap, txt: str
    ) -> bool:
        have_match = False
        for key, value in map_.items():
            if txt.startswith(func(key)):
                GitLabProject.add_labels(labels, value)
                have_match = True
        return have_match

    @staticmethod
    def match_contains(labels: set, func: TextFunc, map_: LabelMap, txt: str) -> bool:
        have_match = False
        for key, value in map_.items():
            if func(key) in txt:
                GitLabProject.add_labels(labels, value)
                have_match = True
        return have_match

    @staticmethod
    def match_re(labels: set, map_: LabelPatternMap, txt: str) -> bool:
        have_match = False
        for key, value in map_.items():
            if key.search(txt):
                GitLabProject.add_labels(labels, value)
                have_match = True
        return have_match

    @staticmethod
    def single_area(txt: str) -> str:
        return txt + ": "

    @staticmethod
    def next_area(txt: str) -> str:
        return " " + txt + ": "

    @staticmethod
    def tag(txt: str) -> str:
        return "[" + txt + "]"

    @staticmethod
    def as_is(txt: str) -> str:
        return txt

    def eval_title(self, labels: set, title: str, config: ProjectConfig.ConfigSet):
        self.match_starts_with(labels, self.single_area, config.areas, title)
        self.match_contains(labels, self.next_area, config.areas, title)
        self.match_contains(labels, self.tag, config.areas, title)
        self.match_contains(labels, self.as_is, config.title_codenames, title)

    def eval_description(self, labels: set, desc: str, config: ProjectConfig.ConfigSet):
        self.match_contains(labels, self.as_is, config.desc_codenames, desc)

    def eval_path(
        self, labels: set, path: str, config: ProjectConfig.ConfigSet
    ) -> bool:
        return self.match_re(labels, config.paths, path)

    def process_issue_or_mr(self, issue, is_issue: bool):
        title = issue.title.lower()
        desc = issue.description

        print(issue.attributes["web_url"] + " | " + escape_term(issue.title))

        if "wip:" in title or "draft:" in title:
            print("skipping draft")
            return

        labels = set()

        config = self.config.issues if is_issue else self.config.mrs

        self.eval_title(labels, title, config)

        if desc is not None:
            self.eval_description(labels, desc.lower(), config)

        # is this a merge request?
        if not is_issue:
            chs = issue.changes()
            for change in chs["changes"]:
                paths = set((change["old_path"], change["new_path"]))
                if not any(self.eval_path(labels, path, config) for path in paths):
                    for path in paths:
                        print("unrecognized path: " + escape_term(path))

        if not self.ignore_label_history and self.token:
            if len(issue.resourcelabelevents.list()) > 0:
                print("has a history of label changes - skipping")
                print("old labels:          " + set2str(issue.labels))
                print("new labels would be: " + set2str(labels))

                return

        if len(labels) == 0:
            print("unable to determine labels")
            return

        self.apply_labels(issue, labels)

    def run_graphql(self, query):
        import requests
        import time

        headers = {}
        if self.token:
            headers["Authorization"] = f"Bearer {self.token}"

        count = 3
        while count > 0:
            count -= 1
            request = requests.post(
                f"{self.config.url}/api/graphql", json={"query": query}, headers=headers
            )
            if request.status_code == 200:
                return request.json()

            if request.status_code >= 500:
                time.sleep(10)
            else:
                raise GitLabError(
                    f"unable to process graphQL query: {request.status_code}"
                )

        raise GitLabError("unable to process graphQL query: timeout")

    def fetch_labels(self) -> list[str]:
        labels = list()

        def fetch_labels_graphql(cursor=""):
            # the graphql query below requests up to 100 labels
            graphql = self.run_graphql(
                f"""
query labels {{
  project(fullPath: "{self.proj.path_with_namespace}") {{
    id
    name
    labels({cursor}includeAncestorGroups: true) {{
      edges {{
        node {{
          title
        }}
      }}
      pageInfo {{
        endCursor
        hasNextPage
      }}
    }}
  }}
}}
"""
            )
            graphql_labels = graphql["data"]["project"]["labels"]

            # store the list of retrieved labels in 'labels'
            labels.extend([e["node"]["title"] for e in graphql_labels["edges"]])

            # walk through the next page, if required
            if graphql_labels["pageInfo"]["hasNextPage"]:
                end_cursor = graphql_labels["pageInfo"]["endCursor"]
                fetch_labels_graphql(cursor=f'after: "{end_cursor}", ')

        fetch_labels_graphql()

        return labels

    def process_issues(self, issue_id: int, since: datetime.datetime | None):
        if issue_id > 0:
            self.process_issue_or_mr(self.proj.issues.get(issue_id), is_issue=True)
            return

        kwargs = {
            "as_list": False,
            "labels": self.label,
            "state": self.state,
            "order_by": "updated_at",
        }
        if since is not None:
            kwargs["updated_after"] = since.isoformat()

        issues = self.proj.issues.list(**kwargs)

        for issue in issues:
            self.process_issue_or_mr(issue, is_issue=True)
            print()

    def process_mrs(self, mr_id: int, since: datetime.datetime | None):
        if mr_id > 0:
            # we gave the tool a specific MR, make sure we have changes associated
            # with it, we will wait ~5 minutes if the MR is fresh and not ready
            timeout = [5, 10, 20, 30, 30, 30, 30, 30, 30, 30, 30, 30]

            for t in timeout:
                gl_mr = self.proj.mergerequests.get(mr_id)
                if len(gl_mr.changes()["changes"]) > 0:
                    break
                time.sleep(t)

            self.process_issue_or_mr(self.proj.mergerequests.get(mr_id), is_issue=False)
            return

        kwargs = {
            "as_list": False,
            "labels": self.label,
            "state": self.state,
            "order_by": "updated_at",
        }
        if since is not None:
            kwargs["updated_after"] = since.isoformat()

        mrs = self.proj.mergerequests.list(**kwargs)
        for mr in mrs:
            self.process_issue_or_mr(mr, is_issue=False)
            print()
